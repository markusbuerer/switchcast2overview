//
//  Event.swift
//  SWITCHcastOverview
//
//  Created by Markus Buerer on 07/10/16.
//  Copyright © 2016 switch. All rights reserved.
//

import Foundation


class Event: NSObject {
    var org = ""
    var state = ""
    var title = ""
    var id = ""
    var workflowStart = Date()
    var currentWorkflow = ""
    var information = ""
    var updated = false
    var seriesTitle = ""
    var seriesID = ""
    var duration = ""
    var progress = 0.0
    var updateDate = Date()
    var currentJobID = ""
    var mediaURL = ""
    var createdDate = ""
    
    
    
    public init(org: String, id: String, title: String, state: String) {
        
        self.updated = true
        self.org = org
        self.id = id
        self.title = title
        self.state = state
        self.information = "Organisation: \(org)\nID: \(id)\nTitle: \(title)\n"
//        self.progress = 20
    }
    
    func resetInformation() {
        self.updated = true
        self.information = "Organisation: \(org)\nID: \(id)\nTitle: \(title)\n\n"
    }

    
}
