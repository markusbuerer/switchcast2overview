//
//  ViewController.swift
//  SWITCHcastOverview
//
//  Created by Markus Buerer on 06/10/16.
//  Copyright © 2016 switch. All rights reserved.
//

import Cocoa
import Alamofire
import SwiftyJSON
import SWXMLHash

class ViewController: NSViewController , NSTableViewDataSource, NSTableViewDelegate{
    
    @IBOutlet var events: NSArrayController!
    @IBOutlet weak var statusLine: NSTextField!
    @IBOutlet weak var numberOfRows: NSTextField!
    @IBOutlet weak var jobTableView: NSTableView!
    @IBOutlet var detailView: NSTextView!
    @IBOutlet weak var currentTime: NSTextField!
    @IBOutlet weak var runningFlag: NSButton!
    @IBOutlet weak var failedFlag: NSButton!
    @IBOutlet weak var mongodbCheckbox: NSButton!
    @IBOutlet weak var orgTableView: NSTableView!
//    @IBOutlet var barChartView: BarChartView!
    @IBOutlet weak var searchField: NSSearchField!
    @IBOutlet weak var healthView: NSView!
    @IBOutlet weak var environmentBox: NSBox!
    @IBOutlet weak var progressView: NSProgressIndicator!
    
    @IBOutlet weak var uuidSearchField: NSSearchField!
    @IBOutlet weak var healthField: NSTextField!
    var nextRefresh = 120
    let dateFormatter = DateFormatter()
    let eventFormatter = DateFormatter()
//    let defaultOrgs = ["unibe-ch"]
//  let defaultOrgs = ["switch-ch"]
    let defaultOrgs = ["tube", "bzpflege-ch", "careum-ch","ethz-ch", "epfl-ch", "fh-hwz-ch", "fhnw-ch", "hepfr-ch", "hepl-ch", "hes-so-ch", "hfh-ch", "hftm-ch","hslu-ch", "insel-ch", "phbern-ch", "phlu-ch", "phsg-ch", "phsz-ch", "phzh-ch", "supsi-ch", "switch-ch", "uni-li", "unibas-ch", "unibe-ch", "unifr-ch", "unil-ch", "unilu-ch", "unine-ch", "usz-ch", "uzh-ch", "zhaw-ch"]
    var organizations: [String] = []
    
    dynamic var eventArray = Array<Event>()
    var originalEventArray = Array<Event>()
    let user = "matterhorn_system_account"
    var password = "Jl-12FaqEzPfd83-fds"
    var urlQuery = "producer.cast.switch.ch"
    let refDate = Date(timeIntervalSince1970: 0)
    var refreshDate = Date()
    var credential: URLCredential
    
    @IBAction func removeFromList(_ sender: NSMenuItem) {
        let index = self.jobTableView.clickedRow
        if index >= 0 {
            self.eventArray.remove(at: index)
        }
        
        
    }
    
    @IBAction func cancelJob(_ sender: NSMenuItem) {
        let index = self.jobTableView.clickedRow
        if index >= 0 {
            let event = self.eventArray[index]
//            print("JobID: \(event.currentJobID)")
            let castUrl = URL(string: "https://switch-ch.\(urlQuery)/workflow/stop")!
                
            let headers: HTTPHeaders = [
                "X-Requested-Auth": "Digest",
                "X-Opencast-Matterhorn-Authorization": "true"
            ]
            let params : Parameters = ["id":event.currentJobID]
            
            Alamofire.request(
                castUrl,
                method: .post,
                parameters: params,
                headers: headers)
                .authenticate(usingCredential: self.credential)
                .validate(statusCode: 200..<300)
                .responseData { response in
                    switch response.result {
                    case .success:
                        event.state = "STOPPED"
                    case .failure(let error):
                        print(error)
                    }
            }

        }
        
        
    }
    
    @IBAction func searchUUID(_ sender: Any) {
        self.detailView.string = ""
        self.jobTableView.deselectAll(self)
        for event in self.eventArray { event.updated = false }
        let requestString = uuidSearchField.stringValue.components(separatedBy: ":")
//        print(requestString)
        
        if requestString.count < 2 {
            self.uuidSearchField.stringValue = ""
            return
        }
        var request1 = requestString[1].trimmingCharacters(in: CharacterSet(charactersIn: "\""))
        request1 = request1.trimmingCharacters(in: CharacterSet.whitespaces)
        request1 = request1.components(separatedBy: " ")[0]
        if request1.isEmpty {
            self.uuidSearchField.stringValue = ""
            return
        }
        self.uuidSearchField.stringValue = "\(requestString[0]):\(request1)"
        let orgHeader = "https://\(requestString[0].trimmingCharacters(in: CharacterSet.whitespaces)).\(urlQuery)/api/events?filter=textFilter:\(request1)"
        //print(orgHeader)
        //        let orgHeader = "https://\(org).\(urlQuery)/api/events?filter=textFilter:a6effc64"
        let castUrl = URL(string: orgHeader)!
//        print(castUrl)
        var req = URLRequest(url: castUrl)
        req.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
        req.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
        self.loadEventsFromServer(request: req, org: requestString[0])

        
    }
    
    @IBAction func downloadArchive(_ sender: NSMenuItem) {
        let index = self.jobTableView.clickedRow
        var downloadFolder = ""
        var newFolder = ""
        var eventFolder = ""
        var url = ""
        if index >= 0 {
            let event = self.eventArray[index]
            if event.org == "tube" {
                self.downloadAsset(event: event)
                return
            }
            let castUrl = URL(string: "https://\(event.org).\(urlQuery)/assets/episode/\(event.id)")!
            var request = URLRequest(url: castUrl)
//            print(castUrl)
            request.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
            request.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")

            Alamofire.request(request)
                .authenticate(usingCredential: self.credential)
                .response { response in
                    eventFolder = event.title.replacingOccurrences(of: ":", with: "-")
                    downloadFolder = self.createFolder(folderName: eventFolder, parent: "")
                    let xml = SWXMLHash.parse(response.data!)
//                    print(xml)
//                    print("\nTracks:")
                    for index in 0 ..< xml["mediapackage"]["media"]["track"].all.count {
//                        print(xml["mediapackage"]["media"]["track"][index].element?.attribute(by: "type")?.text as Any)
                        url = (xml["mediapackage"]["media"]["track"][index]["url"].element?.text)!
                        newFolder = (xml["mediapackage"]["media"]["track"][index].element?.attribute(by: "type")?.text)!
                        newFolder = downloadFolder+"/"+newFolder.replacingOccurrences(of: "/", with: "-")
                        self.downloadAsset(urlString: url, toFolder: newFolder)
                    }
//                    print("\nCatalogs:")
                    for index in 0 ..< xml["mediapackage"]["metadata"]["catalog"].all.count {
//                        print(xml["mediapackage"]["metadata"]["catalog"][index].element?.attribute(by: "type")?.text as Any)
                        url = (xml["mediapackage"]["metadata"]["catalog"][index]["url"].element?.text)!
                        newFolder = (xml["mediapackage"]["metadata"]["catalog"][index].element?.attribute(by: "type")?.text)!
                        newFolder = downloadFolder+"/"+newFolder.replacingOccurrences(of: "/", with: "-")
                        self.downloadAsset(urlString: url, toFolder: newFolder)
                    }
//                    print("\nAttachments:")
                    for index in 0 ..< xml["mediapackage"]["attachments"]["attachment"].all.count {
//                        print(xml["mediapackage"]["attachments"]["attachment"][index].element?.attribute(by: "type")?.text as Any)
                        url = (xml["mediapackage"]["attachments"]["attachment"][index]["url"].element?.text)!
                        newFolder = (xml["mediapackage"]["attachments"]["attachment"][index].element?.attribute(by: "type")?.text)!
                        newFolder = downloadFolder+"/"+newFolder.replacingOccurrences(of: "/", with: "-")
                        self.downloadAsset(urlString: url, toFolder: newFolder)
                        
                    }
                    
            }
            
        }
    }
    
    @IBAction func downloadPublished(_ sender: NSMenuItem) {
        let index = self.jobTableView.clickedRow
        var downloadFolder = ""
        var eventFolder = ""
        // event succeeded?
        if index >= 0 {
            let event = self.eventArray[index]
            if event.org == "tube" {
                self.downloadAsset(event: event)
                return
            }
            let castUrl = URL(string: "https://\(event.org).\(urlQuery)/api/events/\(event.id)?sign=true&withacl=false&withmetadata=false&withpublications=true")!
            var request = URLRequest(url: castUrl)
//            print(castUrl)
            request.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
            request.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
            
            Alamofire.request(request)
                .authenticate(usingCredential: self.credential)
                .responseJSON { response in
                    eventFolder = "\(event.title)_\(event.createdDate)"
                    eventFolder = eventFolder.replacingOccurrences(of: ":", with: "_")
                    downloadFolder = self.createFolder(folderName: eventFolder, parent: "")
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
//                print("\(json)")
                        if json[]["processing_state"] == "SUCCEEDED" {
                            for item in json[]["publications"].arrayValue {
                                if item["channel"] == "switchcast-api" {
    //                            print(item["media"].arrayValue)
                                    for mediaItem in item["media"].arrayValue {
                                        if mediaItem["flavor"] == "delivery/h264-720p" {
                                            //print(mediaItem)
                                            self.downloadAsset(urlString: mediaItem["url"].stringValue, toFolder: downloadFolder)
                                        }
                                    }
                                }
                            }
                        }
                    case .failure(let error):
                        print("Error for: \(error)")
                    }
                    
            }
            
        }

        
        
    }
    
    
    func showCompleted(info: String) {
        let alert = NSAlert()
        alert.messageText = "Download done"
        alert.informativeText = "Files for mediapackage: \(info) were downloaded"
        alert.addButton(withTitle: "OK")
        alert.beginSheetModal(for: view.window!, completionHandler: nil)
    }
    
        
    func createFolder(folderName: String, parent: String)-> String {
        let filemgr = FileManager.default
        var newDir = ""
        if parent.isEmpty {
            let dirPaths = filemgr.urls(for: .downloadsDirectory, in: .userDomainMask)
            let docsURL = dirPaths[0]
            newDir = docsURL.appendingPathComponent(folderName).path
        } else {
            newDir = parent + "/" + folderName.replacingOccurrences(of: "/", with: "-")
        }
//        print("Create Folder: \(newDir)")
        do {
            try filemgr.createDirectory(atPath: newDir,
                                        withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        return newDir
    }

    func downloadAsset(urlString: String, toFolder: String) {
        

        let downloadURL = URL(string: urlString)
        let downloadFileName = downloadURL?.lastPathComponent
        let newDestination = toFolder+"/"+downloadFileName!
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .downloadsDirectory, in: .userDomainMask)[0]
            // the name of the file here I kept is yourFileName with appended extension
            documentsURL.appendPathComponent(downloadFileName!)
            return (documentsURL, [.removePreviousFile])
        }
        
        var request = URLRequest(url: URL(string:urlString)!)
//        print(urlString)
        request.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
        request.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
        
        Alamofire.download(request, to: destination)
            .authenticate(usingCredential: self.credential)
            .response { response in
//            
         if response.destinationURL != nil {
            do {
                try FileManager.default.createDirectory(atPath: toFolder,
                                            withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Error create Dir: \(error.localizedDescription)")
            }
            do {
                try FileManager.default.moveItem(at: response.destinationURL!, to: URL(fileURLWithPath:newDestination))
            } catch let error as NSError {
                print("Error MOVE: \(error.localizedDescription)")
            }
         }
        }
    }
    
    
    func downloadAsset(event: Event) {
        
        if event.mediaURL == "" {return}
        let downloadURL = URL(string: event.mediaURL)
        let downloadFileName = downloadURL?.lastPathComponent
        
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .downloadsDirectory, in: .userDomainMask)[0]
            // the name of the file here I kept is yourFileName with appended extension
            documentsURL.appendPathComponent(downloadFileName!)
            return (documentsURL, [])
        }
        
        var request = URLRequest(url: downloadURL!)
        //        print(downloadURL)
        request.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
        request.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
        
        Alamofire.download(request, to: destination)
            .authenticate(usingCredential: self.credential)
            .response { response in
//                print("I am finished with \(response.destinationURL!)")
        }
    }
    
    @IBAction func openInBrowser(_ sender: NSMenuItem) {
        let index = self.jobTableView.clickedRow
        if index >= 0 {
//            self.eventArray.remove(at: index)
        }
        
        
    }


    
    required init?(coder: NSCoder) {
        credential = URLCredential(user: user, password: password, persistence: .forSession)
        super.init(coder: coder)
        organizations = defaultOrgs
        dateFormatter.dateFormat = "HH:mm:ss"
        eventFormatter.dateFormat = "dd.MM.YY HH.mm.ss"
        
    }
    
    @IBAction func selectEnvironment(_ sender: NSButton) {
        
        switch sender.tag {
        case 0:// production
            password = "Jl-12FaqEzPfd83-fds"
            urlQuery = "producer.cast.switch.ch"
            credential = URLCredential(user: user, password: password, persistence: .forSession)
            self.eventArray = []
            self.view.window?.backgroundColor = NSColor.windowBackgroundColor
        case 1:// test
            password = "dsPCg-3qI83cXv"
            urlQuery = "producer.cast-test.switch.ch"
            credential = URLCredential(user: user, password: password, persistence: .forSession)
            self.eventArray = []
            self.view.window?.backgroundColor = NSColor.yellow
        default: // default = production
            password = "Jl-12FaqEzPfd83-fds"
            urlQuery = "producer.cast.switch.ch"
        }
        self.refreshData(self)
    }


    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.updateStatusLine), userInfo: nil, repeats: true)
//        healthView.layer?.backgroundColor = NSColor.green.cgColor

        self.refreshData(self)

    }
    
    func updateStatusLine() {
        currentTime.stringValue = dateFormatter.string(from: Date())
        nextRefresh = nextRefresh-1
        self.statusLine.stringValue = "Refresh in \(nextRefresh) seconds"
        if nextRefresh == 0 {
            nextRefresh = 120
            self.refreshData(self)
        }
    }

   
    
// MARK: Data load
    
    @IBAction func refreshData(_ sender: AnyObject) {
        
        self.refreshJobTable()
        refreshDate = Date()
        for org in organizations {
            loadEvents(org: org)
        }
//        self.loadHealth()
    }
    
    func loadHealth() {
        let castUrl = URL(string: "https://switch-ch.\(urlQuery)/services/health")!
        var request = URLRequest(url: castUrl)
        request.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
        request.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
//        self.healthField.stringValue = "Services (\(castUrl))"
        Alamofire.request(request)
            .authenticate(usingCredential: self.credential)
            .validate(statusCode: 200..<300)
            .responseData { response in
                switch response.result {
                case .success:
                    self.healthView.layer?.backgroundColor = NSColor.green.cgColor
                case .failure(let error):
                    print(error)
                    self.healthView.layer?.backgroundColor = NSColor.red.cgColor
                }
        }
    }
    
    func loadEvents(org: String) {
        self.detailView.string = ""
        self.jobTableView.deselectAll(self)
        for event in self.eventArray { event.updated = false }
        let requestString = "RUNNING"
//        if runningFlag.state == NSOnState { requestString = "RUNNING" }
//        if failedFlag.state == NSOnState { requestString = requestString + "%20FAILED" }
//
        let orgHeader = "https://\(org).\(urlQuery)/api/events?filter=textFilter:\(requestString)"
//        let orgHeader = "https://\(org).\(urlQuery)/api/events?filter=textFilter:a6effc64"
        let castUrl = URL(string: orgHeader)!
        //    print(castUrl)
        var req = URLRequest(url: castUrl)
        req.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
        req.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
        self.loadEventsFromServer(request: req, org: org)
    }

    
    func loadEventsFromServer(request: URLRequest, org: String) {
        Alamofire.request(request)
            .authenticate(usingCredential: self.credential)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print(json)
                    for item in json[].arrayValue {
                        if self.eventArray.index(where: { $0.id == item["identifier"].stringValue }) != nil {
                            // existing event
                            let oldEvent: Event = self.eventArray.first(where: { $0.id == item["identifier"].stringValue } )!
                            oldEvent.resetInformation()
                            self.loadWorkflowInfosFor(event: oldEvent)
                            oldEvent.updateDate = self.refreshDate
                        } else {
                            // new Event
                            let newEvent = Event(org: org, id: item["identifier"].stringValue, title: item["title"].stringValue, state: item["processing_state"].stringValue)
                            newEvent.createdDate = item["created"].stringValue
                                self.eventArray.append(newEvent)
                            self.loadWorkflowInfosFor(event: newEvent)
                            newEvent.updateDate = self.refreshDate
                        }
                    }
                case .failure(let error):
                    print("Error for \(org): \(error)")
                }
                self.originalEventArray = self.eventArray
                self.numberOfRows.stringValue = "Total: \(self.eventArray.count) row(s)"
        }
        
    }
    
    func refreshJobTable() {
        for event in self.eventArray {
            if !(event.updateDate == self.refreshDate) {
                self.updateEventInfo(event: event)
                if (event.state == "SUCCEEDED") {
                    let index = self.eventArray.index(of: event)
                    self.eventArray.remove(at: index!)
                }
            }
        }
    }
    
    
    func loadWorkflowInfosFor(event: Event) {
//        let eventHeader = "https://\(event.org).\(urlQuery)/workflow/instances.json?q=a6effc64-a403-4a5a-afe9-37800f311355"
        let eventHeader = "https://\(event.org).\(urlQuery)/workflow/instances.json?q=\(event.id)"
//      let eventHeader = "https://\(event.org).\(urlQuery)/workflow/instances.json?state=RUNNING&mp=\(event.id)"
        let castUrl = URL(string: eventHeader)!
//        print(castUrl)
        var req = URLRequest(url: castUrl)
        req.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
        req.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
        
        Alamofire.request(req)
            .authenticate(usingCredential: self.credential)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print(json)
                    event.seriesID = json["workflows"]["workflow"]["mediapackage"]["series"].stringValue
                    event.seriesTitle = json["workflows"]["workflow"]["mediapackage"]["seriestitle"].stringValue
                    event.duration = json["workflows"]["workflow"]["mediapackage"]["duration"].stringValue
                    event.mediaURL = json["workflows"]["workflow"]["mediapackage"]["media"]["track"]["url"].stringValue
                    if event.seriesID != "" {
                        event.information = event.information + "Created: \(event.createdDate)\n\nSeries: \(event.seriesTitle) (ID: \(event.seriesID))\n"
                    } else {
                        event.information = event.information + "Created: \(event.createdDate)\n\n"
                    }
                    
//                    print(event.mediaURL)
                    
                    switch json["workflows"]["workflow"].type {
                        case .dictionary:
                            self.fillEventInfos(event: event, dictionary: json["workflows"]["workflow"].dictionary!)
                        case .array:
                            for item in json["workflows"]["workflow"].arrayValue {
                                self.fillEventInfos(event: event, dictionary: item.dictionary!)
                            }
                        default: break
                    }

/*
                    for item in json["workflows"]["workflow"]["operations"]["operation"].arrayValue {
                        allJobs = allJobs+1
                     

                        if item["state"] == "RUNNING" {
//                            print("Current Workflow: \(item["description"])")
                            event.currentWorkflow = item["description"].stringValue
                            event.workflowStart = start_time as Date
                        }
                        if item["state"] == "FAILED" {
                            event.currentWorkflow = "FAILED: " + item["description"].stringValue
                            event.workflowStart = start_time as Date
                        }
                    }
*/
                    // calculate Process
                    self.jobTableView.reloadData()
                case .failure(let error):
                    print("Error for \(event.id): \(error)")
                }
        }
        
        
        
    }

    func fillEventInfos(event: Event, dictionary: Dictionary<String, JSON>) {
        var allJobs = 0.0
        var doneJobs = 0.0
        var failed = false

        
//        print("\(dictionary["id"]!) : \(dictionary["title"]?.stringValue.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)) :\(dictionary["state"]!)")
        if dictionary["state"]?.stringValue == "RUNNING" {
            event.currentJobID = (dictionary["id"]?.stringValue)!
        }
//        print("\(dictionary["id"]!) : \(dictionary["mediapackage"]!["seriestitle"].stringValue)")
        
        let title = dictionary["title"]?.stringValue.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        event.information = event.information + "\nJob: '\(title!)' : \(dictionary["state"]!) (ID: \(dictionary["id"]!))\n"
        
        for operationItem in (dictionary["operations"]?["operation"].arrayValue)! {
            allJobs = allJobs+1
            let start_time = Date(timeIntervalSince1970: operationItem["started"].doubleValue/1000)
            let stop_time = Date(timeIntervalSince1970: operationItem["completed"].doubleValue/1000)
            if self.refDate < start_time && !failed {
                doneJobs = doneJobs+1
            }
            let wf_time = self.eventFormatter.string(from: start_time)
            event.information = event.information +
            "\(wf_time): \(operationItem["state"].stringValue) : \(operationItem["description"])\n"
            
            if operationItem["state"] == "RUNNING" {
                event.currentWorkflow = operationItem["description"].stringValue
                event.workflowStart = start_time as Date
            } else {
                if self.refDate < stop_time {
                    event.currentWorkflow = operationItem["description"].stringValue
                    event.workflowStart = stop_time as Date
                }
            }

            event.state = dictionary["state"]!.stringValue
            
            if operationItem["state"] == "FAILED" {
                failed = true
            }
        }
        event.progress = 100.0*doneJobs/allJobs

    }
    
    
    func updateEventInfo(event: Event) {
        let castUrl = URL(string: "https://\(event.org).\(urlQuery)/api/events/\(event.id)")!
        var request = URLRequest(url: castUrl)
        request.addValue("Digest", forHTTPHeaderField: "X-Requested-Auth")
        request.addValue("true", forHTTPHeaderField: "X-Opencast-Matterhorn-Authorization")
        Alamofire.request(request)
            .authenticate(usingCredential: self.credential)
            .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        event.state = json["processing_state"].stringValue
                        if (event.state == "SUCCEEDED") {
                            event.progress = 100
                        }
                    default: break
                }
        }
    }




// MARK: table selection

    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        self.events.sortDescriptors = jobTableView.sortDescriptors
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        if notification.object as! NSTableView == self.orgTableView {
            self.eventArray = self.originalEventArray
            let selectionIndexes = self.orgTableView.selectedRowIndexes
            var searchString = ""
            for index in selectionIndexes.enumerated() {
                searchString = searchString+self.organizations[index.element]+" "
            }
            if (searchString == "") {
                numberOfRows.stringValue = "Total: \(self.eventArray.count) row(s)"
                return
            }
            let filteredArray = self.eventArray.filter{ searchString.contains($0.org) }
            numberOfRows.stringValue = "Total: \(filteredArray.count) row(s)"
//            self.eventArray = Set(filteredArray)
            self.eventArray = filteredArray
        }
    }
    

    func tableView(_ tableView: NSTableView, didAdd rowView: NSTableRowView, forRow row: Int) {
        if !self.eventArray.isEmpty {
            if self.eventArray[row].state == "FAILED" {
                rowView.backgroundColor = NSColor.init(red: 1.0, green: 0.5, blue: 0.5, alpha: 0.8)
            } else {
                rowView.backgroundColor = NSColor.clear
            }
        }
        
    }
 

}

